import pytest

from .solutions import set_based_solution

test_cases = [
    [8, 7, 5, 3],
    [4, 5, 15, 2, 8],
    [1, 2, 4, 11, 21, 31, 41, 51, 61, 6],
    [1, 2, 4, 5, 11, 21, 31, 41, 51, 61, 6],
    [1, 2, 4, 11, 21, 31, 41, 51, 61, 111, 311, 100002, 101, 6, 6],
]


def test_solution_for_empty():
    with pytest.raises(TypeError):
        set_based_solution()


def test_for_empty_list():
    with pytest.raises(IndexError):
        set_based_solution([])


def test_for_argument_other_then_list():
    with pytest.raises(TypeError):
        set_based_solution(10)


def test_against_test_cases_for_positive():
    assert set_based_solution(test_cases[0]) == True


def test_against_test_cases_for_negative():
    assert set_based_solution(test_cases[1]) == False


def test_against_big_list():
    assert set_based_solution(test_cases[4]) == True
