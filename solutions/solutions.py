def naive_solution(numbers) -> bool:
    """ 
    solution: This will use naive approach.
    Will do two for loops with O(n^2) time complexity.
    Criteria:Given a list of integers, check if the 
    sum of any two integers in the list is not contained in the list.
    For example with the list [4, 5, 15, 2, 8], there is no pair of
    integers where their sum is in the list. The list [8, 7, 5, 3] 
    does not satisfy the criteria since the sum of 5 and 3 is in the list.
    Write a python function which takes a list of integers and returns True 
    if the list satisfies the criteria above, or False otherwise.

    Parameters:
    numbers (int): list of integers, no type guarantee for this assignment.

    Returns:
    True/False based on found criteria, default is False, exception in cases,
    such as empty list.
    """
    if type(numbers) is not list:
        raise TypeError
    if len(numbers) == 0:
        raise IndexError

    # get length of argument numbers
    len_of_numbers = len(numbers)
    for i in range(0, len_of_numbers):
        for j in range(i + 1, len_of_numbers):
            if (numbers[i] + numbers[j]) in numbers:
                return True
    return False


def sort_solution(numbers) -> bool:
    """ 
    solution: This will use naive approach with sorting before the loops.
    Will do two for loops with O(n^2) time complexity.
    Criteria:Given a list of integers, check if the 
    sum of any two integers in the list is not contained in the list.
    For example with the list [4, 5, 15, 2, 8], there is no pair of
    integers where their sum is in the list. The list [8, 7, 5, 3] 
    does not satisfy the criteria since the sum of 5 and 3 is in the list.
    Write a python function which takes a list of integers and returns True 
    if the list satisfies the criteria above, or False otherwise.

    Parameters:
    numbers (int): list of integers, no type guarantee for this assignment.

    Returns:
    True/False based on found criteria, default is False, exception in cases,
    such as empty list.
    """
    if type(numbers) is not list:
        raise TypeError
    if len(numbers) == 0:
        raise IndexError

    # sort numbers
    numbers.sort()
    # get length of argument numbers
    len_of_numbers = len(numbers)
    for i in range(0, len_of_numbers):
        for j in range(i + 1, len_of_numbers):
            if (numbers[i] + numbers[j]) in numbers:
                return True
    return False


def hashing_solution(numbers) -> bool:
    """ 
    solution: This will use naive approach with hashing before the loops.
    Will do two for loops with O(n^2) time complexity.
    Criteria:Given a list of integers, check if the 
    sum of any two integers in the list is not contained in the list.
    For example with the list [4, 5, 15, 2, 8], there is no pair of
    integers where their sum is in the list. The list [8, 7, 5, 3] 
    does not satisfy the criteria since the sum of 5 and 3 is in the list.
    Write a python function which takes a list of integers and returns True 
    if the list satisfies the criteria above, or False otherwise.

    Parameters:
    numbers (int): list of integers, no type guarantee for this assignment.

    Returns:
    True/False based on found criteria, default is False, exception in cases,
    such as empty list.
    """
    if type(numbers) is not list:
        raise TypeError
    if len(numbers) == 0:
        raise IndexError

    # create hash table for argument given
    hashed_numbers = {i: 1 for i in numbers}

    # get length of argument numbers
    len_of_numbers = len(numbers)
    for i in range(0, len_of_numbers):
        for j in range(i + 1, len_of_numbers):
            if (numbers[i] + numbers[j]) in hashed_numbers.keys():
                return True
    return False


def set_based_solution(numbers) -> bool:
    """ 
    solution: This will use naive approach with using Set before the loops to remove duplicates.
    Will do two for loops with O(n^2) time complexity.
    Criteria:Given a list of integers, check if the 
    sum of any two integers in the list is not contained in the list.
    For example with the list [4, 5, 15, 2, 8], there is no pair of
    integers where their sum is in the list. The list [8, 7, 5, 3] 
    does not satisfy the criteria since the sum of 5 and 3 is in the list.
    Write a python function which takes a list of integers and returns True 
    if the list satisfies the criteria above, or False otherwise.

    Parameters:
    numbers (int): list of integers, no type guarantee for this assignment.

    Returns:
    True/False based on found criteria, default is False, exception in cases,
    such as empty list.
    """
    if type(numbers) is not list:
        raise TypeError
    if len(numbers) == 0:
        raise IndexError

    # create set for argument given to remove duplicates
    numbers_set = set(numbers)

    # get length of argument numbers
    len_of_numbers = len(numbers)
    for i in range(0, len_of_numbers):
        for j in range(i + 1, len_of_numbers):
            if (numbers[i] + numbers[j]) in numbers_set:
                return True
    return False
