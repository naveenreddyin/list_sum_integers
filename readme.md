# Problem
> Given a list of integers, check if the 
    sum of any two integers in the list is not contained in the list.
    For example with the list [4, 5, 15, 2, 8], there is no pair of
    integers where their sum is in the list. The list [8, 7, 5, 3] 
    does not satisfy the criteria since the sum of 5 and 3 is in the list.
    Write a python function which takes a list of integers and returns True 
    if the list satisfies the criteria above, or False otherwise.

# Prerequisite 
- Install requirements, by running command (assuming that virtualenv is activated):
  - `pip install requirements.txt -r`
- Run tests, by issuing command:
  - `pytest --durations=0`
 
# Benchmarking
### Using pythons inbuild timeit modul
- On naive solution:
  > `In [14]: timeit.timeit(stmt='naive_solution([1, 2, 4, 11, 21, 31, 41, 51, 61, 111, 311, 100002, 101, 6])', setup='from solutions.solutions import naive_solution', number=10000)`
 `Out[14]: 0.05309089100001074`

- On sort solution:
  > `In [12]: timeit.timeit(stmt='sort_solution([1, 2, 4, 11, 21, 31, 41, 51, 61, 111, 311, 100002, 101, 6])', setup='from solutions.solutions import sort_solution', number=10000)`
    `Out[12]: 0.056590133000000264`
- On hashing solution:
  > `In [8]: timeit.timeit(stmt='hashing_solution([1, 2, 4, 11, 21, 31, 41, 51, 61, 111, 311, 100002, 101, 6])', setup='from solutions.solutions import hashing_solution', number=10000)`
 `Out[8]: 0.050256216000001075`

- On set based solution:
  > `In [5]: timeit.timeit(stmt='set_based_solution([1, 2, 4, 11, 21, 31, 41, 51, 61, 111, 311, 100002, 101, 6])', setup='from solutions.solutions import set_based_solution', number=10000)`
 `Out[5]: 0.03016154499999857`


